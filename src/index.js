const puppeteer = require('puppeteer');
const _cliProgress = require('cli-progress');
var spinner = require("./step");
var qrcode = require('qrcode-terminal');
var path = require("path");

var knex = require('knex')({
  client: 'mysql',
  connection: {
    host : 'localhost',
    user : 'root',
    password : 'iamroot4521',
    database : 'riset_wabot'
  }
});

function getDb(call){
    return knex('queque_send').where('sended', 0).select("*").limit(1).then(call);
}

function updateDb(id){
    return knex('queque_send')
      .where('id',id)
      .update({
        sended: 1
      });
}

async function Main() {

    try {
        var page;
        await downloadAndStartThings();
        var isLogin = await checkLogin();
        if (!isLogin) {
            await getAndShowQR();
        }
    } catch (e) {
        console.error("Looks like you got an error.");
        page.screenshot({ path: path.join(process.cwd(), "error.png") })
        console.error("Don't worry errors are good. They help us improve. A screenshot has already been saved as error.png in current directory. Please mail it on vasani.arpit@gmail.com along with the steps to reproduce it.");
        throw e;
    }

    /**
     * If local chrome is not there then this function will download it first. then use it for automation. 
     */
    async function downloadAndStartThings() {
        spinner.start("Downloading chrome\n");
        const browserFetcher = puppeteer.createBrowserFetcher({
            path: process.cwd()
        });
        const progressBar = new _cliProgress.Bar({}, _cliProgress.Presets.shades_grey);
        progressBar.start(100, 0);

        const revisionInfo = await browserFetcher.download("619290", (download, total) => {
            var percentage = (download * 100) / total;
            progressBar.update(percentage);
        });
        progressBar.update(100);
        spinner.stop("Downloading chrome ... done!");
        //console.log(revisionInfo.executablePath);
        spinner.start("Launching Chrome");
        const browser = await puppeteer.launch({
            executablePath: revisionInfo.executablePath,
            headless: true,
            devtools: false

        });
        spinner.stop("Launching Chrome ... done!");
        spinner.start("Opening Whatsapp");
        page = await browser.pages();
        if (page.length > 0) {
            page = page[0];
            page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36")
            await page.goto('https://web.whatsapp.com', {
                waitUntil: 'networkidle0',
                timeout: 0
            });
            //console.log(contents);
            var filepath = path.join(__dirname, "WAPI.js");
            await page.addScriptTag({path: require.resolve(filepath)});
            spinner.stop("Opening Whatsapp ... done!");
        }
        page.on('console', consoleObj => console.log(consoleObj.text()));
    }

    async function sendMessageWhatApp(){
        var result = getDb( async(result) => {
            for(var i in result){
                await page.evaluate(
                    "WAPI.sendMessageToID('"+result[i].nomer_id+"@c.us','"+result[i].message+"')"
                );
                updateDb(result[i].id).then( result => { return result; });
                console.log('Mengirm pesan ke '+result[i].nomer_id);
                console.log('Dengan Pesan '+result[i].message);
            }
        });
    }

    async function checkLogin() {
        spinner.start("Page is loading");
        var output = await page.evaluate("WAPI.isLoggedIn();");
        if (output) {
            spinner.stop("Looks like you are already logged in");
            console.log(await page.evaluate("window.chrome;"));
            console.log(await page.evaluate("window.outerWidth;"));
            console.log(await page.evaluate("window.outerHeight;"));

        } else {
            spinner.info("You are not logged in. Please scan the QR below");
        }
        return output;
    }

    async function getAndShowQR() {
        var imageData = await page.evaluate(`document.querySelector("img[alt='Scan me!']").parentElement.getAttribute("data-ref")`);
        qrcode.generate(imageData, { small: true });
        spinner.start("Waiting for scan \nKeep in mind that it will expire after few seconds");
        var isLoggedIn = await page.evaluate("WAPI.isLoggedIn();");
        while (!isLoggedIn) {
            isLoggedIn = await page.evaluate("WAPI.isLoggedIn();");
        }
        if (isLoggedIn) {
            spinner.stop("Looks like you are logged in now");
            await page.evaluate('WAPI.getAllGroups(function(result){ console.log(result); });')
            setInterval(sendMessageWhatApp, 1000); //every 1/2 second
            console.log("Welcome, WBOT is up and running");
        }
    }

}

Main();
